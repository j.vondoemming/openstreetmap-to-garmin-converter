#!/bin/bash

set -e

if [ "$#" != "1" ]; then
	echo "Usage: $0 <SOURCE .osm.pbf FILE>"
	exit 1
fi

src="${1}"

java -cp req/splitter/lib/ -jar req/splitter/splitter.jar "${src}"
