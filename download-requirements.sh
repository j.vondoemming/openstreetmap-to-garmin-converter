#!/bin/bash

set -e

mkdir -p req
rm -rv req
mkdir -p req

pushd req


# Splitter
echo "Splitter"
wget https://www.mkgmap.org.uk/download/splitter-r652.zip -O splitter.zip
unzip splitter.zip
rm splitter.zip
mv -v splitter* splitter

# Mkgmap
echo "Mkgmap"
wget https://www.mkgmap.org.uk/download/mkgmap-r4905.zip -O mkgmap.zip
unzip mkgmap.zip
rm mkgmap.zip
mv -v mkgmap* mkgmap

# Osmconvert
echo "Osmconvert"
#wget http://m.m.i24.cc/osmconvert64 -O osmconvert
#chmod +x osmconvert
wget -O - http://m.m.i24.cc/osmconvert.c | cc -x c - -lz -O3 -o osmconvert

# Osmfilter
echo "Osmfilter"
wget -O - http://m.m.i24.cc/osmfilter.c |cc -x c - -O3 -o osmfilter

popd
