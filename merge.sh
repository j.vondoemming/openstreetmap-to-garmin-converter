#!/bin/bash

set -e

echo "=> Merging"
#java -cp req/mkgmap/lib/ -jar req/mkgmap/mkgmap.jar "${src}"
java -cp req/mkgmap/lib/ -jar req/mkgmap/mkgmap.jar \
    --route \
    --latin1 \
    --style-file=os50_style_and_TYP.zip \
    --add-pois-to-areas \
    --bounds=bounds \
    --index \
    --gmapsupp \
    6324*.osm.pbf

echo "-> Done"
