#!/bin/bash

set -e

if [ "$#" != "1" ]; then
	echo "Usage: $0 <SOURCE .osm.pbf FILE>"
	echo "You can download one here: http://download.geofabrik.de/europe.html"
	exit 1
fi

src="${1}"

date

[ ! -d req ] && ./download-requirements.sh || true

./download-style.sh

./split.sh "${1}"
./create-bounds.sh "${1}"
./merge.sh

echo "=> Done."
date
