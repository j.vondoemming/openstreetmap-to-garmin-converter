#!/bin/bash

echo ""
echo ""
echo ""
echo "============= WARNING ============="
echo ""
echo " This will delete every direct file in this directory!"
echo " Including: gmapsupp.img and your-source.osm.pbf"
echo " Create Backups!"
echo ""
echo "============= WARNING ============="
echo ""
echo ""
echo "Waiting 20 seconds..."
echo "Hit Ctrl+C to cancel."

sleep 10

echo "10"
sleep 5
echo "5"
sleep 1
echo "4"
sleep 1
echo "3"
sleep 1
echo "2"
sleep 1
echo "1"
sleep 1
echo "0"

echo "Deleting files..."

rm  *.osm.pbf *.osm *.o5m *.img template.args areas.list map-boundaries.o5m  map.o5m  areas.poly osmmap.tdb densities-out.txt
rm -r bounds

echo "Done."
