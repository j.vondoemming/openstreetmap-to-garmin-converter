#!/bin/bash

set -e

if [ "$#" != "1" ]; then
	echo "Usage: $0 <SOURCE .osm.pbf FILE>"
	exit 1
fi

src="${1}"

echo "=> Create Bounds"

echo "-> Converting"
./req/osmconvert "${src}" -o=map.o5m

echo "-> Filtering"
./req/osmfilter map.o5m \
    --keep-nodes= \
    --keep-ways-relations="boundary=administrative =postal_code postal_code=" \
    -o=map-boundaries.o5m

echo "-> Preprocessing"
#java -cp req/mkgmap/lib/ -jar req/mkgmap/mkgmap.jar "${src}"
java -cp req/mkgmap/lib/:req/mkgmap/mkgmap.jar \
    uk.me.parabola.mkgmap.reader.osm.boundary.BoundaryPreprocessor \
    map-boundaries.o5m \
    bounds

echo "-> Done"
