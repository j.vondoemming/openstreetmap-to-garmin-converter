# OpenStreetMap to Garmin Converter

## Usage

You can download a source file here: http://download.geofabrik.de/europe.html

```
./all.sh <SOURCE .osm.pbf FILE>
```

This will produce a `gmapsupp.img` File which you can put on the SD Card of your Garmin Device as `/Garmin/gmapsupp.img`.

WARNING: This tool will create many large files!

You can remove all files (including your source file and `gmapsupp.img`) using `./clean-all.sh`.

## Links

### Wichtige

- https://wiki.openstreetmap.org/wiki/Mkgmap/help/How_to_create_a_map
- http://download.geofabrik.de/europe.html

### Sonstige

- https://wiki.openstreetmap.org/wiki/Downloading_data
- https://wiki.openstreetmap.org/wiki/Use_OpenStreetMap
- https://www.mkgmap.org.uk/
- https://wiki.openstreetmap.org/wiki/Mkgmap
- https://wiki.openstreetmap.org/wiki/Planet.osm#Country_and_area_extracts
- https://wiki.openstreetmap.org/wiki/Mkgmap/help/options
- https://www.pinns.co.uk/osm/styles.html

